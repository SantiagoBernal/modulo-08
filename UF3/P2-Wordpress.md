## Pràctica 2: 

Seleccioneu una empresa real que tingui similitud amb algun dels tres casos que heu treballat a la pràctica de cerca d'informació (Restaurant, Botiga online, Startup de projectes).

Quina empresa heu seleccionat?

* Jofre: SquareEnix (Squaresoft)

* Kilian: Steam

* Uiliam: Blizzard

* Santi: Cooler Master

* Dani: McDonalds

* Manu: VelscoTattoo

Un cop seleccioneu la marca/empresa podeu instal·lar Wordpress i començar a personalitzar la web amb les opcions que heu investigat (plugins).

**Obligatori:** Mostrar captura i/o explicació. 

* Canviar el Tema per defecte instal·lant-ne un.

![](Images/Wordpress/Tema.png)

* Instal·lar 5 plugins (justificant perquè els instal·leu).

***Woocomerce:*** Añade una gran variedad de utilidades de compra online, además de añadir una serie de paginas editables de "Tienda", además de poder añadir varios productos de forma fácil, añadir una descripción y permitir que los clientes realicen reseñas de los productos.

***MetaSliders:*** Permite la elaboración de Sliders personalizables, de esta forma se pueden enseñar con facilidad las categorias de la tienda

***Paypal buy now button:*** Facilita las compras mediante Paypal añadiendo un botón de compra, se instala ya que acelera el proceso de compra y hace más comodo para los usuarios que compran mediante paypal realizar el pago. 

![](Images/Wordpress/PLUGINS.png)

* Crear un perfil de cada tipus que permeti Wordpress. Explicar què pot fer cada tipus de perfil.

* Canvia o afegeix algun canvi CSS a un Tema. Com ho has fet?
![](Images/Wordpress/Tema_editado.png)
