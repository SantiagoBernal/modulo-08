![Alt](Images/Pinche_feo.jpg "")

# Datos Personales:

**Nombre:** Santiago Bernal.

**Edad:** 17 años.

**Dirección:** c/ de Rogent #132, Principal #2.

**Número Telefónico:** 633 96 70 57

**Correo electrónico:** ar843234@iespoblenou.org


## Estudios:

* **CFGM Sistemes Microinformatics i Xarxes En el Institut Poblenou (Cursando).** 

* **Educación Secundaria Obligatoria (ESO).**

* **Curso pre universitario de ingenieria Informatica (Universidad del Quindío Colombia).**

## Conocimientos Informáticos:

* **Conocimiento nivel usuario de Linux.**
* **Alto conocimiento del paquete ofimático de LibreOffice.**
* **Conocimiento en instalación de sistemas operativos.**
* **Conocimiento en instalación y mantenimiento de Hardware.**
* **Conocimiento básico en Python**
* **Autodidacta en Unity.** 

## Idiomas:

| **Idiomas**   | **Nivel**     | 
|---------------|:-------------:|
| Inglés        |      B2       |
| Castellano    |    Nativo     |
| Catalán       |      B2       |

## Hobbies e Intereses:

* **Programación de pequeños proyectos en Unity.**
* **Dibujo en digital.**
* **Tengo interés en el desarrollo creativo y programación de los videojuegos.**

## Competencias transversales:

* Responsable.
* Flexible.
* Comprometido.
* Resuelvo problemas de forma creativa y lógica.
* Puedo trabajar bajo presión.
* Autodidacta.
* Sociable y Jovial.


