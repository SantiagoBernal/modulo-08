# P2. Eines Ofimàtiques Web

Durant 4 hores treballarem P2. 

Continuant l' informe iniciat en P1, ara toca incloure eines ofimàtiques web. 

S'han d'incloure els següents punts.

Per cada punt escollit s'ha d'introduir en l'informe igual que en P1.

> És important:
>> Organitzar-se amb el company per repartir-se la feina.
>> Explorar a Internet qualsevol punt que no sabem com funciona.
>> Per a cada punt anar redactant l'informe que el professor avaluarà.
>> Els punts en vermell són obligatoris que estiguin en l'informe.


### Comparació entre els diferents paquets ofimàtics web

1.Fes una **taula comparativa** entre els diferents sistemes i inclou-lo en l'informe. 

Indica quines eines té cada paquet ofimàtic web, apreciareu moltes diferències

+ Google Drive
+ Office 365
+ Zoho Docs
+ ThinkFree
+ Feng Office 

Exemple:

Aplicació | Google Drive | Office 365
---|---|---
Processador de text | sí | no
Fulla de càlcul | no | sí

...

2.Un cop vistes les eines que podeu usar:

Escull entre Zoho Docs, Office 365, Google Drive i Feng Office i explica la **majoria d'aplicacions** que conté el paquet, també les menys conegudes. (Llistat d'aplicacions i captura del seu funcionament) 

Intenteu que tingui relació amb la vostra empresa.


Exemple: Escullo Zoho Docs


**Aplicació:** Backstage

**Link:** [Examen SMIX](https://prova1.zohobackstage.eu/ExamenSMIX)

**Funcionament:**

![images](images/1.png)

![images](images/2.png)



3.Tot seguit en el Google Drive creeu una carpeta de projecte on existeixi com a mínim dos documents col·laboratius, en PODEU POSAR MÉS:
+ Una **carta de presentació** on mostreu qui sou, a quina empresa representeu i quins serveis oferiu.
+ Un **pressupost simulat** per un projecte  de la vostra empresa.
+ Un **formulari de contacte web** per a la vostra empresa i un lloc on guardar-ne els resultats.


#### Infogràfics

4.Easel.ly: Realitza un infogràfic útil per a la teva empresa. És important que es vegi en l'infogràfic la informació important remarcada, utilitza aquesta pàgina web, o una altra que hagis descobert que faci la mateixa funció. CAPTURES i explicació en l'informe.

5.Finalment mitjançant una eina com prezzi o slideshare fes una presentació que permeti veure en profunditat l'estructura de la teva organització.

En qualsevol moment dins l'informe podeu incloure qualsevol contingut web significatiu com poden ser imatges o vídeos.

